library(outlier96)
library(bnutil)


getdata = function() {
  result = AnnotatedData$new(data =outlier96::df, metadata = outlier96::mdf)
  return(result)
}

setResult = function(annotatedResult){

  print(annotatedResult)

  result = annotatedResult$data

}

bnMessageHandler = bnshiny::BNMessageHandler$new()
bnMessageHandler$getDataHandler = getdata
bnMessageHandler$setResultHandler = setResult


bnshiny::startBNTestShiny('outlier96', sessionType='run', bnMessageHandler=bnMessageHandler)
